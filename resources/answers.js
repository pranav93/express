const logger = require('./../utils/logger');

function createAnswers(req, res) {
  logger.log('info', `create answer for question id ${req.params.qID}`);
  return res.json({
    response: 'a POST request for CREATING answers',
    question: req.params.qID,
    body: req.body,
  });
}

function modifyAnswers(req, res) {
  return res.json({
    response: 'a PUT request for EDITING answers',
    question: req.params.qID,
    answer: req.params.aID,
    body: req.body,
  });
}

function deleteAnswers(req, res) {
  return res.json({
    response: 'a DELETE request for DELETING answers',
    question: req.params.qID,
    answer: req.params.aID,
    body: req.body,
  });
}

function voteAnswers(req, res) {
  return res.json({
    response: 'a POST request for VOTING on answers',
    question: req.params.qID,
    answer: req.params.aID,
    vote: req.params.dec,
    body: req.body,
  });
}

exports.createAnswers = createAnswers;
exports.modifyAnswers = modifyAnswers;
exports.deleteAnswers = deleteAnswers;
exports.voteAnswers = voteAnswers;
