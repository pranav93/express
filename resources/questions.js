function listQuestions(req, res) {
  console.log('listQuestions');
  return res.send('listQuestions');
}

function createQuestion(req, res) {
  console.log('createQuestion');
  return res.send('createQuestion');
}

function getQuestionByID(req, res) {
  console.log(`a GET request for LOOKING at a special answer id: ${req.params.qID}`);
  return res.send(`a GET request for LOOKING at a special answer id: ${req.params.qID}`);
}

exports.listQuestions = listQuestions;
exports.createQuestion = createQuestion;
exports.getQuestionByID = getQuestionByID;
