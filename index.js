const express = require('express');
const bodyParser = require('body-parser');
const questionRouter = require('./routes/questions');
const answerRouter = require('./routes/answers');
const logger = require('./utils/logger');
const morgan = require('morgan');

const app = express();

app.use(bodyParser.json());

app.use('/questions', questionRouter);
app.use('/questions', answerRouter);

logger.log('info', 'Overriding \'Express\' logger');

app.use(morgan('combined', { stream: logger.stream }));

app.listen(3000, () => console.log('Example app is running on 3000!'));
