const express = require('express');

const router = express.Router();

const answerResource = require('./../resources/answers');


// Create an answers to a question.
router.post('/:qID/answers/', (req, res) => answerResource.createAnswers(req, res));

// Modify an answer to a question.
router.put('/:qID/answers/:aID', (req, res) => answerResource.modifyAnswers(req, res));

// Delete an answer to a question.
router.delete('/:qID/answers/:aID', (req, res) => answerResource.deleteAnswers(req, res));

// Vote an answer.
router.post('/:qID/answers/:aID/vote-:dec', (req, res) => answerResource.voteAnswers(req, res));

module.exports = router;
