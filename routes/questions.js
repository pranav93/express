const express = require('express');
const questionResource = require('./../resources/questions');

const router = express.Router();

// Get all questions.
router.get('/', (req, res) => questionResource.listQuestions(req, res));

// Get a question from id.
router.get('/:qID', (req, res) => questionResource.getQuestionByID(req, res));

// Create a question.
router.post('/', (req, res) => questionResource.createQuestion(req, res));

module.exports = router;
